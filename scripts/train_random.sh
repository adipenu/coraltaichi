#!/bin/bash

set -euo pipefail

export DATA_DIR=./Synthetic_NeRF
# PATH FOR HIPERGATOR
# /home/eherrin/git/miniconda3/bin/conda run python train_coral.py \
# FOR NOT HIPERGATOR
python3 train_coral.py \
    --root_dir $DATA_DIR/Lego_aug \
    --exp_name Lego_aug \
    --batch_size 1024 --lr 1e-2 --gui \
    #--half2_opt \
    #--val_only --ckpt_path ckpts/nsvf/Lego/epoch=19-v6.ckpt
    # NOTE: Bash size cut in half
