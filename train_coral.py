from train import *
import subprocess

def main():

    #train_base_nerf() # trains a NeRF using 120 images

    # Ordered_candidate_IG next_best_view_estimator(img[], pose[], )
    # Path_plot,  poses_along_path Path_planner(ordered_candidate_IG)
   # create_random_augmented_dataset(source_directory = "~/Synthetic_NeRF/Lego", new_name = "Lego_aug", image_num = 20)

    train_random_augmented_nerf()

    # train_augmented_nerf()


if __name__ == '__main__':
    main()